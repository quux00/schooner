# 13-Apr
* QUESTION Should followers send AEResp after a heartbeat AEReq ??
** Maybe yes, but then the peer handlers should send the response to the leader_loop ?
* TODO Need to read go-raft code to see how it handles heartbeat logic 
* IDEA Could have a separate heartbeat thread and when get msg from leader_loop that a message is sent, reset the timer

# 05-Apr
* DONE Add in concept of peers
* DONE If a follower gets a client request, does it have the information needed to send it to the leader?
* Starting to add stateMachine (goraft useless as usual)
* DONE How does a follow know the commit_idx at startup?
* DONE Does the commit_idx need to be written to disk somewhere? Or is it assumed to be the prev_log_idx and revised as more information comes in?

# 22-Mar
* Have implemented truncate and tests passing.
* Tried to change STOP message, but tests were failing (network_listener thread was shutting down, but not the follower thread)
* Where to pick up next: read through AER RPC rules in raft.pdf carefully and outline what else needs to be done
** TODO more test around proper follow log logic
** Have not implement anything around "commited" stated to the StateMachine => need to design how that will work
* Have only implemented follower logic
* Need to implement some Leader logic in terms of receiving from clients and sending to followers
* Follower state servers need to know when a client request is coming in, not a leader message
** QUESTION leaders send AEReqs, what do clients send?  Need to define a protocol/format for that.  What does goraft do?

# 13-Mar
* Create a task to listen on socket 23158 -> takes a Chan to put Events on (maybe just pure JSON messages for now?)
* Peers will communicate via JSON (goraft uses protocol buffers)
* server_loop will grab from the event Port
* Chan is now clonable => if doesnt' work, may need to upgrade compiler (11)



# Misc Rust Notes
* Arc<uint> and unsafely cast a &uint to a &mut AtomicBool
* Vec::new() is the same as ~[]
