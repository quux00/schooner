schooner
========

An implementation of the [Raft distributed consensus algorithm](https://ramcloud.stanford.edu/wiki/download/attachments/11370504/raft.pdf) in Rust.

### Status

I have begun work on it for the Spring 2014 session of the [cs4414 Operating Systems course](http://rust-class.org/) being taught in Rust.  It will be my semester class project.  Since it is for the course, I have to keep the code private until the course is finished, at which point it will be open sourced and posted here in April or May 2014.

### Background

Other links about the Raft protocol:

* http://raftconsensus.github.io/
* https://www.youtube.com/watch?v=YbZ3zDzDnrw
* http://www.infoq.com/presentations/raft
  * [Slides for this presentation](https://speakerdeck.com/benbjohnson/raft-the-understandable-distributed-consensus-protocol)
* http://www.reddit.com/comments/1jm6c8


### LICENSE

The library will licensed under the permissive [MIT License](http://opensource.org/licenses/MIT).

